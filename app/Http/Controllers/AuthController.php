<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
      return view('halaman.form');
    }

    public function welcome(Request $request)
    {
       $firstname = $request['firstname'];

       return view('halaman.welcome', compact('firstname'));
    }
}
