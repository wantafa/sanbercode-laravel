@extends('layout.master')
@section('judul')
    Halaman Form
@endsection
@section('isi')
  <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>

   <form action="/kirim" method="post">
   @csrf
    <div class="form-group">
        <label for="">First Name:</label>
        <br><br>
        <input type="text" class="form-control" name="firstname">
        <br><br>
        <label for="">Last Name:</label>
        <br><br>
        <input type="text" class="form-control" name="lastname">
   </div>
<br>
        <div class="form-group">
            <label class="form-check-label">Gender:</label><br><br>
            <input type="radio" name="male">
            Male <br>
            <input type="radio" name="female">
            Female <br>
            <input type="radio" name="other">
            Other
          </label>
        </div>
<br>
        <div class="form-group">
          <label for="">Nationality:</label><br><br>
          <select class="form-control" name="nationality">
            <option>Indonesian</option>
            <option>Singapore</option>
            <option>Malaysian</option>
            <option>Australian</option>
          </select>
        </div>

<br>

    <div class="form-check">
      <label class="form-check-label">Language Spoken: </label> <br><br>
        <input type="checkbox" class="form-check-input" name="bahasa_indonesia">
        Bahasa Indonesia<br>
        <input type="checkbox" class="form-check-input" name="english">
        English<br>
        <input type="checkbox" class="form-check-input" name="other">
        Other
    </div>

<br>

    <div class="form-group">
      <label for="">Bio:</label><br><br>
      <textarea class="form-control" name="bio" id="" rows="3"></textarea>
    </div>

        
        <br>
  
  <button type="submit">Submit</button>
</form> 
@endsection