<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use App\Http\Controllers\HomeController;
// use App\Http\Controllers\CastController;

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index');
Route::get('/form', 'AuthController@register');
Route::post('/kirim', 'AuthController@welcome');

Route::get('/admin', 'HomeController@admin');
Route::get('/data-tables', 'HomeController@datatable');
Route::get('/table', 'HomeController@table');

// CAST
// Route::get('/cast', 'CastController@index');
// Route::get('/cast/create', 'CastController@create');
// Route::post('/cast', 'CastController@store');
// Route::get('/cast/{cast_id}', 'CastController@show');
// Route::get('/cast/{cast_id}/edit', 'CastController@edit');
// Route::put('/cast/{cast_id}', 'CastController@update');
// Route::delete('/cast/{cast_id}', 'CastController@destroy');

Route::resource('cast', 'CastController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
